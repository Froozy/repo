package repositories.impl;

import java.sql.*;
import java.util.List;

import repositories.IUserRepository;
import unitofwork.IUnitOfWork;
import domain.Role;
import domain.User;

public class UserRepository 
extends Repository<User> implements IUserRepository{

	public UserRepository(Connection connection, IEntityBuilder<User> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}
	protected String getTableName() {
		return "users";
	}
	protected String getUpdateQuery() {
		return 
				"UPDATE users SET (login,password)=(?,?) WHERE id=?";
	}
	protected String getInsertQuery() {
		return "INSERT INTO users(login,password)"
				+ "VALUES(?,?)";
	}
	protected void setUpInsertQuery(User entity) throws SQLException {
		insert.setString(1, entity.getLogin());
		insert.setString(2, entity.getPassword());
	}
	protected void setUpUpdateQuery(User entity) throws SQLException {
		update.setString(1, entity.getLogin());
		update.setString(2, entity.getPassword());
		update.setInt(3, entity.getId());	
	}
	public List<User> withRole(int roleId) {
		// TODO Auto-generated method stub
		return null;
	}
	public List<User> withRole(Role role) {
		// TODO Auto-generated method stub
		return null;
	}
	public List<User> withRole(String roleName) {
		// TODO Auto-generated method stub
		return null;
	}

}