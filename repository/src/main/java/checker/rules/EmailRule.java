package checker.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import checker.CheckResult;
import checker.RuleResult;
import domain.Person;

public class EmailRule {
	

	public boolean isEmailValid(String email)
	{
	boolean	result;
	Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
	Matcher m = p.matcher(email);

	boolean matchFound = m.matches();

	if (matchFound) {
	     result=true;
	} else {
	     result=false;
	}
	return result;
	}
	public CheckResult checkRule(Person entity) {
		if(entity.getEmail()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getEmail().equals(""))
			return new CheckResult("", RuleResult.Error);
		if (!isEmailValid(entity.getEmail()))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
		
	}

}