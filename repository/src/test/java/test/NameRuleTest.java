package test;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NameRule;
import domain.Person;

public class NameRuleTest {

	NameRule rule = new NameRule();
	
	@Test
	public void checker_if_name(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_if_empty(){
		Person p = new Person();
		p.setFirstName("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_ok_if_name(){
		Person p = new Person();
		p.setFirstName("Jan");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	
	//...reszta testów

}
