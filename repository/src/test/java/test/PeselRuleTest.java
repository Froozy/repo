package test;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.PeselRule;
import domain.Person;

public class PeselRuleTest {

	PeselRule rule = new PeselRule();
	
	@Test
	public void checker_If_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_If_empty(){
		Person p = new Person();
		p.setPesel("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_Pesel(){
		Person p = new Person();
		p.setPesel("77031375320");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
}